#include "../CaloCellPedestalCorr.h"
#include "../CaloCellNeighborsAverageCorr.h"
#include "../CaloCellRandomizer.h"
#include "../CaloCellRescaler.h"
#include "../CaloCellEnergyRescaler.h"
#include "../CaloCellTimeCorrTool.h"


DECLARE_COMPONENT( CaloCellPedestalCorr )
DECLARE_COMPONENT( CaloCellNeighborsAverageCorr )
DECLARE_COMPONENT( CaloCellRandomizer )
DECLARE_COMPONENT( CaloCellRescaler )
DECLARE_COMPONENT( CaloCellEnergyRescaler )
DECLARE_COMPONENT( CaloCellTimeCorrTool )

